// [SECTION] Dependencies and Modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');


// [SECTION] Environment Variables Setup
	// configure the application in order for it to recognize and identify the necessary components needed to build the app successfully.
	dotenv.config();
	// extract the variables from the .env file.
	// verify the variable by displaying its value in the console. make sure to identify the origin of the component.
	const secret = process.env.CONNNECTION_STRING;


// [SECTION] Server Setup
const port = process.env.PORT;
const app = express();

// middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// [SECTION] Database Connect
// mongoDB connection
mongoose.connect(secret,
			{
				useNewUrlParser : true,
				useUnifiedTopology : true
			}
	);
	// the 'process' keyword
let db = mongoose.connection;
db.on('error', console.error.bind(console, "Connection Error"));
db.once('open', () => console.log("Successfully connected to the database"));

// [SECTION] Server Routes
app.use('/users', userRoutes);
app.use('/courses', courseRoutes);

// [SECTION] Server Response
app.get('/', (req, res) => {
	res.send('hosted in heroku')
})
app.listen(port, () => console.log(`Server running at port ${port}`));
