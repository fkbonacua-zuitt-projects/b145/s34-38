const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers')
const auth = require('../auth');

// Checking Email
router.post("/checkEmail", (req, res) => {
		userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// Registration
router.post('/register', (req, res) => {
		userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))

});

// Login

router.post('/login', (req, res) => {
		userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))

});

// Activity
// // 1. 
// router.get('/details', (req, res) => {
// 		userController.getUser(req.params.id,).then(resultFromController => res.send(resultFromController))
// })

// // Get all users
router.get("/", (req, res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
});

// Solution - Retieve specific details
router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

router.post("/enroll", auth.verify, (req, res) => {

	let data = {

		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;