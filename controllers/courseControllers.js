const Course = require('../models/Course');

const bcrypt = require('bcrypt');

const auth = require('../auth');

// Add a course

// module.exports.addCourse = (reqBody) => {

// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	});

// 	return newCourse.save().then((course, err) => {

// 		if (err) {
// 			return false;
// 		} else	{
// 			return course;
// 		}
// 	})
// }

// activity refactor

// module.exports.addCourse = (reqBody, isAdmin) => {

// 	if (isAdmin) {

// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price,
// 	});

// 		return newCourse.save().then((course, err) => {

// 			if (err) {
// 				return false;
// 			} else	{
// 				return true;
// 			}
// 		})
// 	}
// 	return Promise.resolve('Not an Admin');
// };

module.exports.addCourse = async (data) => {
console.log(data)
	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {

			// Course creation successful
			if (error) {

				return false;

			// Course creation failed
			} else {

				return `Course added ${course}`;

			};

		});

	// User is not an admin
	} else {
		return false;
	};
	
};


// retrieving all courses

module.exports.getAllCourses = async (user) => {
		if (user.isAdmin === true) {
			return Course.find({}).then(result => {
				return result
			})
		} else {
			return `${user.email} is not authorized`
		}
}

// retrieval of active courses
module.exports.getAllActive = () => {
		return Course.find({isActive : true}).then(result => {
			return result
		})
}

// retrieval of a specific course

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}

// updating a course

module.exports.updateCourse = (data) => {
	console.log(data)
	return Course.findById(data.courseId).then((result, err) => {
		if (data.payload.isAdmin === true) {

			let updatedCourse = {
				name : data.updatedCourse.name,
				description : data.updatedCourse.description,
				price : data.updatedCourse.price
			}

			return result.save().then((updatedCourse, err) => {
				if (err) {
					return false
				} else {
					return updatedCourse
				}
			})
		} else {
			return false
		}
	})
}

// activity part 4
// archiving a course
module.exports.deleteCourse = (courseId) => {
	return Course.findByIdAndRemove(courseId).then((removedCourse, err) => {
		if (err) {
			console.log(err)
			return false
		} else {
			return removedCourse
		}
	})
}

